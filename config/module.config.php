<?php

namespace DoctrineFileModule;

return [
    'doctrine-file' => [],
    'router' => [
        'routes' => [
            'doctrine-file' => [
                'type' => \Zend\Mvc\Router\Http\Literal::class,
                'options' => [
                    'route' => '/file',
                    'defaults' => [
                        'controller' => Controller\FileController::class,
                    ],
                ],
                'may_terminate' => false,
                'child_routes' => [
                    'file' => [
                        'type' => \Zend\Mvc\Router\Http\Segment::class,
                        'options' => [
                            'route' => '/:entity[/:path]',
                            'constraints' => [
                                'entity' => '[\da-z\-]+',
                                'path' => '.+',
                            ],
                            'defaults' => [
                                'action' => 'index',
                            ],
                        ],
                    ],
                    'viewport' => [
                        'type' => \Zend\Mvc\Router\Http\Segment::class,
                        'options' => [
                            'route' => '/:entity/:viewport/:path',
                            'constraints' => [
                                'entity' => '[\da-z\-]+',
                                'viewport' => '[\da-z\-]+',
                                'path' => '.+',
                            ],
                            'defaults' => [
                                'action' => 'viewport',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            Service\File\FileService::class => Service\File\FileServiceFactory::class,
            \League\Flysystem\MountManager::class => Service\Flysystem\MountManagerFactory::class,
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\FileController::class => Service\Controller\FileControllerFactory::class,
        ],
    ],
];
