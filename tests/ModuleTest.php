<?php

namespace DoctrineFileModuleTest;

class ModuleTest extends \PHPUnit_Framework_TestCase
{

    public function testGetConfig()
    {
        $this->assertTrue(file_exists(__DIR__ . '/../config/module.config.php'));

        $module = new \DoctrineFileModule\Module();
        $config = $module->getConfig();

        $this->assertTrue(is_array($config));
        $this->assertArrayHasKey('doctrine-file', $config);
    }

    public function testGetAutoloaderConfig()
    {
        $module = new \DoctrineFileModule\Module();
        $config = $module->getAutoloaderConfig();

        $this->assertTrue(is_array($config));
    }

}
