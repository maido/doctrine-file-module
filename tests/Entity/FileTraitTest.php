<?php

namespace DoctrineFileModule\Entity;

function uniqid()
{
    return '57066203abf33';
}

function md5()
{
    return '2dcaf8fe558766857f3c76963e411679';
}

namespace DoctrineFileModuleTest;

class FileTraitTest extends \PHPUnit_Framework_TestCase
{

    /**
     * 
     * @dataProvider generateIdProvider
     */
    public function testGenerateId($path, $expected)
    {
        $trait = $this->getMockForTrait(\DoctrineFileModule\Entity\FileTrait::class);
        $this->assertEquals($expected, $trait->generateId($path));
    }

    public function generateIdProvider()
    {
        return [
            ['path', 'path'],
            ['path/to/file.txt', 'path-to-file-txt'],
        ];
    }

    public function testGenerateIdWithPathNull()
    {
        $trait = $this->getMockForTrait(\DoctrineFileModule\Entity\FileTrait::class);

        $this->assertEquals('57066203abf33', $trait->generateId());

        $trait->setPath('path/to/somewhere');
        $this->assertEquals('path-to-somewhere', $trait->generateId());
    }

    public function testSetIdOnPrePersist()
    {
        $trait = $this->getMockForTrait(\DoctrineFileModule\Entity\FileTrait::class);
        $trait->setPath('path/to/somewhere');
        $trait->setIdOnPrePersist();

        $this->assertEquals('path-to-somewhere', $trait->getId());
    }

    public function testSetCreatedOnPrePersist()
    {
        $trait = $this->getMockForTrait(\DoctrineFileModule\Entity\FileTrait::class);
        $trait->setCreatedOnPrePersist();

        $this->assertInstanceOf(\DateTime::class, $trait->getCreated());

        return $trait;
    }

    public function testSetUpdatedOnPrePersist()
    {
        $trait = $this->getMockForTrait(\DoctrineFileModule\Entity\FileTrait::class);
        $trait->setUpdatedOnPrePersist();

        $this->assertInstanceOf(\DateTime::class, $trait->getUpdated());

        return $trait;
    }

    public function testSetUpdatedOnPreUpdate()
    {
        $trait = $this->getMockForTrait(\DoctrineFileModule\Entity\FileTrait::class);
        $trait->setUpdatedOnPreUpdate();

        $this->assertInstanceOf(\DateTime::class, $trait->getUpdated());
    }

    public function testSetVersionOnPrePersist()
    {
        $trait = $this->getMockForTrait(\DoctrineFileModule\Entity\FileTrait::class);

        $trait->setVersionOnPrePersist();
        $this->assertEquals(1, $trait->getVersion());

        return $trait;
    }

    /**
     * @depends testSetVersionOnPrePersist
     */
    public function testSetVersionOnPreUpdate($trait)
    {
        $trait->setVersionOnPreUpdate();
        $this->assertEquals(2, $trait->getVersion());
    }

    /**
     * @depends testSetCreatedOnPrePersist
     */
    public function testSetEtagOnPrePersist($trait)
    {
        $trait->setEtagOnPrePersist();
        $this->assertEquals('2dcaf8fe558766857f3c76963e411679', $trait->getEtag());
    }

    /**
     * @depends testSetUpdatedOnPrePersist
     */
    public function testSetEtagOnPreUpdate($trait)
    {
        $trait->setEtagOnPreUpdate();
        $this->assertEquals('2dcaf8fe558766857f3c76963e411679', $trait->getEtag());
    }

    public function testSetId()
    {
        $trait = $this->getMockForTrait(\DoctrineFileModule\Entity\FileTrait::class);

        $id = 'some-id';
        $trait->setId($id);
        $this->assertEquals($id, $trait->getId());

        return $trait;
    }

    public function testSetCreated()
    {
        $trait = $this->getMockForTrait(\DoctrineFileModule\Entity\FileTrait::class);

        $created = new \DateTime();
        $trait->setCreated($created);
        $this->assertSame($created, $trait->getCreated());

        return $trait;
    }

    public function testSetUpdated()
    {
        $trait = $this->getMockForTrait(\DoctrineFileModule\Entity\FileTrait::class);

        $updated = new \DateTime();
        $trait->setUpdated($updated);
        $this->assertSame($updated, $trait->getUpdated());

        return $trait;
    }

    public function testSetPath()
    {
        $trait = $this->getMockForTrait(\DoctrineFileModule\Entity\FileTrait::class);

        $path = 'path/to/file';
        $trait->setPath($path);
        $this->assertSame($path, $trait->getPath());

        return $trait;
    }

    public function testSetVersion()
    {
        $trait = $this->getMockForTrait(\DoctrineFileModule\Entity\FileTrait::class);

        $version = 666;
        $trait->setVersion($version);
        $this->assertSame($version, $trait->getVersion());

        return $trait;
    }

    public function testSetEtag()
    {
        $trait = $this->getMockForTrait(\DoctrineFileModule\Entity\FileTrait::class);

        $etag = \md5(\uniqid());
        $trait->setEtag($etag);
        $this->assertSame($etag, $trait->getEtag());

        return $trait;
    }

    public function testSetSize()
    {
        $trait = $this->getMockForTrait(\DoctrineFileModule\Entity\FileTrait::class);

        $size = 323454;
        $trait->setSize($size);
        $this->assertSame($size, $trait->getSize());

        return $trait;
    }

    public function testSetMimetype()
    {
        $trait = $this->getMockForTrait(\DoctrineFileModule\Entity\FileTrait::class);

        $mimetype = 'image/jpeg';
        $trait->setMimetype($mimetype);
        $this->assertSame($mimetype, $trait->getMimetype());

        return $trait;
    }

    public function testSetFilesystem()
    {
        $trait = $this->getMockForTrait(\DoctrineFileModule\Entity\FileTrait::class);

        $filesystem = 'default';
        $trait->setFilesystem($filesystem);
        $this->assertSame($filesystem, $trait->getFilesystem());

        return $trait;
    }

    public function testSetMetadata()
    {
        $trait = $this->getMockForTrait(\DoctrineFileModule\Entity\FileTrait::class);

        $metadata = ['width' => 320, 'height' => 480];
        $trait->setMetadata($metadata);
        $this->assertSame($metadata, $trait->getMetadata());

        return $trait;
    }

}
