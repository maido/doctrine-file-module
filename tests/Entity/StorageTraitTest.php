<?php

namespace DoctrineFileModuleTest;

class StorageTraitTest extends \PHPUnit_Framework_TestCase
{

    public function testSetId()
    {
        $trait = $this->getMockForTrait(\DoctrineFileModule\Entity\StorageTrait::class);

        $id = 'just-unique-id';
        $trait->setId($id);
        $this->assertEquals($id, $trait->getId());
    }

    public function testSetContent()
    {
        $trait = $this->getMockForTrait(\DoctrineFileModule\Entity\StorageTrait::class);

        $content = 'the content to be stored';
        $trait->setContent($content);
        $this->assertEquals($content, $trait->getContent());
    }

}
