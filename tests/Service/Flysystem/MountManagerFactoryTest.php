<?php

namespace DoctrineFileModuleTest\Service\Controller;

class MountManagerFactoryTest extends \PHPUnit_Framework_TestCase
{

    public function testCreateService()
    {
        $services = $this->getMockBuilder(\Zend\ServiceManager\ServiceLocatorInterface::class)
                ->disableOriginalConstructor()
                ->getMock();
        $services->expects($this->once())
                ->method('get')
                ->with('config')
                ->willReturn([]);

        $factory = new \DoctrineFileModule\Service\Flysystem\MountManagerFactory();
        $this->assertInstanceOf(\League\Flysystem\MountManager::class, $factory->createService($services));
    }

}
