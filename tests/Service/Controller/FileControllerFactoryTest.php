<?php

namespace DoctrineFileModuleTest\Service\Controller;

class FileControllerFactoryTest extends \PHPUnit_Framework_TestCase
{

    public function testCreateService()
    {
        $fileService = $this->getMockBuilder(\DoctrineFileModule\Service\File\FileService::class)
                ->disableOriginalConstructor()
                ->getMock();

        $locator = $this->getMockBuilder(\Zend\ServiceManager\ServiceLocatorInterface::class)
                ->disableOriginalConstructor()
                ->getMock();
        $locator->expects($this->once())
                ->method('get')
                ->with(\DoctrineFileModule\Service\File\FileService::class)
                ->willReturn($fileService);

        $controllers = $this->getMockBuilder(\Zend\Mvc\Controller\ControllerManager::class)
                ->disableOriginalConstructor()
                ->getMock();
        $controllers->expects($this->once())
                ->method('getServiceLocator')
                ->willReturn($locator);

        $factory = new \DoctrineFileModule\Service\Controller\FileControllerFactory();
        $this->assertInstanceOf(\DoctrineFileModule\Controller\FileController::class, $factory->createService($controllers));
    }

}
