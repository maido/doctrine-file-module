<?php

namespace DoctrineFileModule\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Http\Response\Stream;
use Zend\Http\Headers;
use DoctrineFileModule\Service\File\FileServiceInterface;

class FileController extends AbstractActionController
{

    /**
     * @var \DoctrineFileModule\Service\File\FileServiceInterface
     */
    protected $fileService;

    /**
     * Class constructor
     *
     * @param FileServiceInterface $fileService
     */
    public function __construct(FileServiceInterface $fileService)
    {
        $this->fileService = $fileService;
    }

    /**
     * Index action
     *
     * @return \Zend\Http\Response
     */
    public function indexAction()
    {
        $entity = $this->params()->fromRoute('entity');
        $path = $this->params()->fromRoute('path');

        $request = $this->getRequest();

        if ($request->isPost()) {
            $files = $request->getFiles();
            $form = $this->fileService->getForm($entity);
            $form->setData($files);

            if ($form->isValid()) {
                $data = $form->getData();
                $file = $this->fileService->create($entity, $data[$entity]);
                $this->response->setStatusCode(201);
                $this->response->setContent(json_encode($file));
            } else {
                $problem = new \DoctrineRestModule\Rest\ApiProblem(400, 'Invalid file upload; see additional details.', null, null, $form->getMessages());
                $this->response->setStatusCode(400);
                $this->response->setContent(json_encode($problem->toArray()));
            }
            return $this->response;
        }

        if (null === $file = $this->fileService->get($entity, $path)) {
            return $this->notFoundAction();
        }

        return $this->prepareResponse($file);
    }

    /**
     * Viewport action
     *
     * @return \Zend\Http\Response
     */
    public function viewportAction()
    {
        $entity = $this->params()->fromRoute('entity');
        $viewport = $this->params()->fromRoute('viewport');
        $path = $this->params()->fromRoute('path');

        if (null === $file = $this->fileService->getViewport($viewport, $entity, $path)) {
            return $this->notFoundAction();
        }

        return $this->prepareResponse($file);
    }

    /**
     * Prepare response
     *
     * @param array $file
     * @return \Zend\Stdlib\ResponseInterface
     */
    public function prepareResponse(array $file)
    {
        if (is_resource($file['contents'])) {
            $response = new Stream();
            $response->setStream($file['contents']);
            $response->setContentLength($file['size']);
            $response->setStreamName(basename($file['path']));
        } else {
            $response = $this->response;
            $response->setContent($file['contents']);
        }

        $headers = new Headers();
        $headers->addHeaders([
            'Last-Modified' => $file['updated']->format('c'),
            'ETag' => $file['etag'],
            'Content-Type' => $file['mimetype'],
            'Content-Length' => $file['size'],
        ]);
        $response->setHeaders($headers);

        return $response;
    }

}
