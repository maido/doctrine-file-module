<?php

namespace DoctrineFileModule\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements DoctrineFileExceptionInterface
{
    
}
