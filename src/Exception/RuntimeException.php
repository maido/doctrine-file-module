<?php

namespace DoctrineFileModule\Exception;

class RuntimeException extends \RuntimeException implements DoctrineFileExceptionInterface
{
    
}
