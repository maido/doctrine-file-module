<?php

namespace DoctrineFileModule\Entity;

interface FileInterface
{

    /**
     * Set id
     *
     * @param string $id
     *
     * @return FileInterface
     */
    public function setId($id);

    /**
     * Get id
     *
     * @return string
     */
    public function getId();

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return FileInterface
     */
    public function setCreated($created);

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated();

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return FileInterface
     */
    public function setUpdated($updated);

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated();

    /**
     * Set path
     *
     * @param string $path
     *
     * @return FileInterface
     */
    public function setPath($path);

    /**
     * Get path
     *
     * @return string
     */
    public function getPath();

    /**
     * Set version
     *
     * @param integer $version
     *
     * @return FileInterface
     */
    public function setVersion($version);

    /**
     * Get version
     *
     * @return integer
     */
    public function getVersion();

    /**
     * Set etag
     *
     * @param string $etag
     *
     * @return FileInterface
     */
    public function setEtag($etag);

    /**
     * Get etag
     *
     * @return string
     */
    public function getEtag();

    /**
     * Set size
     *
     * @param integer $size
     *
     * @return FileInterface
     */
    public function setSize($size);

    /**
     * Get size
     *
     * @return integer
     */
    public function getSize();

    /**
     * Set mimetype
     *
     * @param string $mimetype
     *
     * @return FileInterface
     */
    public function setMimetype($mimetype);

    /**
     * Get mimetype
     *
     * @return string
     */
    public function getMimetype();

    /**
     * Set filesystem
     *
     * @param string $filesystem
     *
     * @return FileInterface
     */
    public function setFilesystem($filesystem);

    /**
     * Get filesystem
     *
     * @return string
     */
    public function getFilesystem();

    /**
     * Set metadata
     *
     * @param array $metadata
     *
     * @return FileInterface
     */
    public function setMetadata(array $metadata = null);

    /**
     * Get metadata
     *
     * @return array
     */
    public function getMetadata();

    /**
     * To array
     *
     * @return array
     */
    public function toArray();
}
