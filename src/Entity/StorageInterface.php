<?php

namespace DoctrineFileModule\Entity;

class StorageInterface
{

    /**
     * Set id
     *
     * @param string $id
     *
     * @return StorageInterface
     */
    public function setId($id);

    /**
     * Get id
     *
     * @return string
     */
    public function getId();

    /**
     * Set content
     *
     * @param string $content
     *
     * @return StorageInterface
     */
    public function setContent($content);

    /**
     * Get content
     *
     * @return resource
     */
    public function getContent();
}
