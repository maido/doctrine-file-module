<?php

namespace DoctrineFileModule\Entity;

use Doctrine\ORM\Mapping as ORM;

trait StorageTrait
{

    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column()
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="blob")
     */
    private $content;

    /**
     * Set id
     *
     * @param string $id
     *
     * @return \DoctrineFileModule\Entity\StorageInterface
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return \DoctrineFileModule\Entity\StorageInterface
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
}
