<?php

namespace DoctrineFileModule\Entity;

use Doctrine\ORM\Mapping as ORM;

trait FileTrait
{

    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column()
     */
    private $id;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @var string
     * @ORM\Column(unique=true)
     */
    private $path;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $version;

    /**
     * @var string
     * @ORM\Column()
     */
    private $etag;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $size;

    /**
     * @var string
     * @ORM\Column()
     */
    private $mimetype;

    /**
     * @var string
     * @ORM\Column(nullable=true)
     */
    private $filesystem;

    /**
     * @var array
     * @ORM\Column(type="array", nullable=true)
     */
    private $metadata;

    /**
     * Generate id
     *
     * @param string $path
     * @return string
     */
    public function generateId($path = null)
    {
        if ($path === null) {
            $path = $this->path;
        }

        $id = preg_replace('/[^\pL\d]+/u', '-', $path);
        $id = iconv('utf-8', 'us-ascii//TRANSLIT', $id);
        $id = preg_replace('/[^-\w]+/', '', $id);
        $id = trim($id, '-');
        $id = preg_replace('~-+~', '-', $id);
        $id = strtolower($id);

        if ($id === '') {
            $id = uniqid();
        }

        return $id;
    }

    /**
     * Set id on pre persist
     *
     * @ORM\PrePersist
     * @return void
     */
    public function setIdOnPrePersist()
    {
        if ($this->id === null) {
            $this->id = $this->generateId();
        }
    }

    /**
     * Set created on pre persist
     *
     * @ORM\PrePersist
     * @return void
     */
    public function setCreatedOnPrePersist()
    {
        $this->created = new \DateTime();
    }

    /**
     * Set updated on pre persist
     *
     * @ORM\PrePersist
     * @return void
     */
    public function setUpdatedOnPrePersist()
    {
        $this->updated = new \DateTime();
    }

    /**
     * Set updated on pre update
     *
     * @ORM\PreUpdate
     * @return void
     */
    public function setUpdatedOnPreUpdate()
    {
        $this->updated = new \DateTime();
    }

    /**
     * Set version on pre persist
     *
     * @ORM\PrePersist
     * @return void
     */
    public function setVersionOnPrePersist()
    {
        if ($this->version === null) {
            $this->version = 1;
        }
    }

    /**
     * Set version on pre update
     *
     * @ORM\PreUpdate
     * @return void
     */
    public function setVersionOnPreUpdate()
    {
        $this->version++;
    }

    /**
     * Set etag on pre persist
     *
     * @ORM\PrePersist
     * @return void
     */
    public function setEtagOnPrePersist()
    {
        $hash = $this->path . $this->version . $this->created->format('c');
        $this->etag = md5($hash);
    }

    /**
     * Set etag on pre update
     *
     * @ORM\PreUpdate
     * @return void
     */
    public function setEtagOnPreUpdate()
    {
        $hash = $this->path . $this->version . $this->updated->format('c');
        $this->etag = md5($hash);
    }

    /**
     * Set id
     *
     * @param string $id
     *
     * @return \DoctrineFileModule\Entity\FileInterface
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return \DoctrineFileModule\Entity\FileInterface
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return \DoctrineFileModule\Entity\FileInterface
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return \DoctrineFileModule\Entity\FileInterface
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set version
     *
     * @param integer $version
     *
     * @return \DoctrineFileModule\Entity\FileInterface
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return integer
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set etag
     *
     * @param string $etag
     *
     * @return \DoctrineFileModule\Entity\FileInterface
     */
    public function setEtag($etag)
    {
        $this->etag = $etag;

        return $this;
    }

    /**
     * Get etag
     *
     * @return string
     */
    public function getEtag()
    {
        return $this->etag;
    }

    /**
     * Set size
     *
     * @param integer $size
     *
     * @return \DoctrineFileModule\Entity\FileInterface
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return integer
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set mimetype
     *
     * @param string $mimetype
     *
     * @return \DoctrineFileModule\Entity\FileInterface
     */
    public function setMimetype($mimetype)
    {
        $this->mimetype = $mimetype;

        return $this;
    }

    /**
     * Get mimetype
     *
     * @return string
     */
    public function getMimetype()
    {
        return $this->mimetype;
    }

    /**
     * Set filesystem
     *
     * @param string $filesystem
     *
     * @return \DoctrineFileModule\Entity\FileInterface
     */
    public function setFilesystem($filesystem)
    {
        $this->filesystem = $filesystem;

        return $this;
    }

    /**
     * Get filesystem
     *
     * @return string
     */
    public function getFilesystem()
    {
        return $this->filesystem;
    }

    /**
     * Set metadata
     *
     * @param array
     *
     * @return \DoctrineFileModule\Entity\FileInterface
     */
    public function setMetadata(array $metadata = null)
    {
        $this->metadata = $metadata;

        return $this;
    }

    /**
     * Get metadata
     *
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * To array
     *
     * @return array
     */
    public function toArray()
    {
        $hydrator = new \Zend\Hydrator\ClassMethods();
        return $hydrator->extract($this);
    }

}
