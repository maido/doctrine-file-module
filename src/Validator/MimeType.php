<?php

namespace DoctrineFileModule\Validator;

class MimeType extends \Zend\Validator\File\MimeType
{

    /**
     * @var array
     */
    protected $messageTemplates = [
        self::FALSE_TYPE => "File has an incorrect type.",
        self::NOT_DETECTED => "The type could not be detected.",
        self::NOT_READABLE => "File is not readable or does not exist.",
    ];

}
