<?php

namespace DoctrineFileModule\Validator;

class ImageSize extends \Zend\Validator\File\ImageSize
{

    /**
     * @var array
     */
    protected $messageTemplates = [
        self::WIDTH_TOO_BIG => "Maximum allowed width for image should be '%maxwidth%' but '%width%' detected.",
        self::WIDTH_TOO_SMALL => "Minimum expected width for image should be '%minwidth%' but '%width%' detected.",
        self::HEIGHT_TOO_BIG => "Maximum allowed height for image should be '%maxheight%' but '%height%' detected.",
        self::HEIGHT_TOO_SMALL => "Minimum expected height for image should be '%minheight%' but '%height%' detected",
        self::NOT_DETECTED => "The size of image could not be detected.",
        self::NOT_READABLE => "File is not readable or does not exist.",
    ];

}
