<?php

namespace DoctrineFileModule\Validator;

class IsImage extends \Zend\Validator\File\IsImage
{

    /**
     * @var array
     */
    protected $messageTemplates = [
        self::FALSE_TYPE   => "Image not found.",
        self::NOT_DETECTED => "Image not found.",
        self::NOT_READABLE => "File is not readable or does not exist.",
    ];

}
