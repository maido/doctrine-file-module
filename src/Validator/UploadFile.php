<?php

namespace DoctrineFileModule\Validator;

class UploadFile extends \Zend\Validator\File\UploadFile
{

    /**
     * @var array
     */
    protected $messageTemplates = [
        self::INI_SIZE => "File exceeds maximum size.",
        self::FORM_SIZE => "File exceeds maximum size.",
        self::PARTIAL => "File was only partially uploaded.",
        self::NO_FILE => "File was not uploaded.",
        self::NO_TMP_DIR => "No directory was found for file.",
        self::CANT_WRITE => "File can't be written.",
        self::EXTENSION => "Error while uploading the file.",
        self::ATTACK => "File was illegally uploaded.",
        self::FILE_NOT_FOUND => "File was not found.",
        self::UNKNOWN => "Unknown error while uploading file.",
    ];

}
