<?php

namespace DoctrineFileModule\Validator;

class Size extends \Zend\Validator\File\Size
{

    /**
     * @var array
     */
    protected $messageTemplates = [
        self::TOO_BIG   => "Maximum allowed size for file is '%max%'.",
        self::TOO_SMALL => "Minimum expected size for file is '%min%' but '%size%' detected.",
        self::NOT_FOUND => "File is not readable or does not exist.",
    ];

}
