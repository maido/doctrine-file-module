<?php

namespace DoctrineFileModule\Service\Flysystem;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use League\Flysystem\MountManager;

class MountManagerFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $services
     * @return \DoctrineFileModule\Service\File\FileService
     */
    public function createService(ServiceLocatorInterface $services)
    {
        $config = $services->get('config');
        $filesystems = isset($config['doctrine-file']['filesystems']) && is_array($config['doctrine-file']['filesystems']) ?
                $config['doctrine-file']['filesystems'] : [];

        $mountManager = new MountManager();

        foreach ($filesystems as $prefix => $factory) {
            $filesystem = $services->get($factory);
            $mountManager->mountFilesystem($prefix, $filesystem);
        }

        return $mountManager;
    }
}
