<?php

namespace DoctrineFileModule\Service\Controller;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;
use DoctrineFileModule\Controller\FileController;
use DoctrineFileModule\Service\File\FileService;

class FileControllerFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $controllers
     * @return FileController
     */
    public function createService(ServiceLocatorInterface $controllers)
    {
        $locator = $controllers->getServiceLocator();
        $fileService = $locator->get(FileService::class);

        $controller = new FileController($fileService);

        return $controller;
    }
}
