<?php

namespace DoctrineFileModule\Service\File;

use Doctrine\ORM\EntityManager;
use DoctrineFileModule\Entity\FileInterface;
use League\Flysystem\MountManager;
use Zend\Stdlib\Hydrator\ClassMethods;

class FileService implements FileServiceInterface
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    /**
     * @var MountManager
     */
    protected $mountManager;

    /**
     * @var array
     */
    protected $entities;

    /**
     * Class constructor
     *
     * @param EntityManager $entityManager
     * @param MountManager $mountManager
     * @param array $entities
     */
    public function __construct(EntityManager $entityManager, MountManager $mountManager, array $entities)
    {
        $this->entityManager = $entityManager;
        $this->mountManager = $mountManager;
        $this->entities = $entities;
    }

    /**
     * Get entity manager
     *
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * Get mount manager
     *
     * @return \League\Flysystem\MountManager
     */
    public function getMountManager()
    {
        return $this->mountManager;
    }

    /**
     * Get entities
     *
     * @return array
     */
    public function getEntities()
    {
        return $this->entities;
    }

    /**
     * Has entity
     *
     * @param string $entity
     * @return bool
     */
    public function hasEntity($entity)
    {
        if (isset($this->entities[$entity])) {
            return true;
        }

        foreach ($this->entities as $name => $config) {
            if (isset($config['class']) && $config['class'] === $entity) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get entity
     *
     * @param string $entity
     * @return array
     * @throws \InvalidArgumentException
     */
    public function getEntity($entity)
    {
        if (!$this->hasEntity($entity)) {
            $message = sprintf('Undefined entity with name "%s".', $entity);
            throw new \InvalidArgumentException($message);
        }

        if (isset($this->entities[$entity])) {
            return $this->entities[$entity];
        }

        foreach ($this->entities as $name => $config) {
            if (isset($config['class']) && $config['class'] === $entity) {
                return $config;
            }
        }
    }

    /**
     * Get entity class
     *
     * @param string $entity
     * @return string
     * @throws \RuntimeException
     */
    public function getEntityClass($entity)
    {
        $config = $this->getEntity($entity);

        if (!isset($config['class']) || !is_string($config['class'])) {
            $message = sprintf('Missing or bad class configuration for entity with name "%s".', $entity);
            throw new \RuntimeException($message);
        }

        return $config['class'];
    }

    /**
     * Get entity name
     *
     * @param string $entity
     * @return string
     * @throws \RuntimeException
     */
    public function getEntityName($entity)
    {
        foreach ($this->entities as $name => $config) {
            if (isset($config['class']) && $config['class'] === $entity) {
                return $name;
            } elseif (is_string($config) && $config === $entity) {
                return $name;
            }
        }

        $message = sprintf('Missing or bad class configuration for entity "%s".', $entity);
        throw new \RuntimeException($message);
    }

    /**
     * Get entity filesystem
     *
     * @param string $entity
     * @return \League\Flysystem\FilesystemInterface
     * @throws \RuntimeException
     */
    public function getEntityFilesystem($entity)
    {
        $config = $this->getEntity($entity);

        if (!isset($config['filesystem']) || !is_string($config['filesystem'])) {
            $message = sprintf('Missing or bad filesystem configuration for entity with name "%s".', $entity);
            throw new \RuntimeException($message);
        }

        return $this->mountManager->getFilesystem($config['filesystem']);
    }

    /**
     * Get
     *
     * @param string $entity
     * @param string $path
     * @return array|null
     */
    public function get($entity, $path)
    {
        $repository = $this->getEntityManager()
                ->getRepository($this->getEntityClass($entity));

        if (null === $file = $repository->findOneByPath($path)) {
            return null;
        }

        $filesystem = $this->getEntityFilesystem($entity);
        $path = $entity . '/' . $file->getPath();

        if (!$filesystem->has($path)) {
            return null;
        }

        $hydrator = new ClassMethods();

        $data = $hydrator->extract($file);
        $data['contents'] = $filesystem->readStream($path);
        $data['path'] = $path;

        return $data;
    }

    public function generateName($original)
    {
        $pos = strrpos($original, '.');
        $extension = is_int($pos) ? substr($original, $pos) : '';
        return uniqid() . $extension;
    }

    public function create($entity, $file)
    {
        $name = $this->generateName($file['name']);

        /* @var $instance FileInterface */
        $class = $this->getEntityClass($entity);
        $instance = new $class($name);
        $contents = file_get_contents($file['tmp_name']);

        $this->persist($instance, $contents, true);
        return $instance->toArray();
    }

    /**
     * Get form
     *
     * @param string $entity
     * @return \Zend\Form\Form
     */
    public function getForm($entity)
    {
        $form = new \Zend\Form\Form();
        $form->add(new \Zend\Form\Element\File($entity));
        $form->setInputFilter($this->getInputFilter($entity));

        return $form;
    }

    protected function getInputFilter($entity)
    {
        $spec = [
            $entity => [
                'validators' => [
                    ['name' => \DoctrineFileModule\Validator\UploadFile::class],
                ],
            ],
        ];

        $config = $this->getEntity($entity);
        if (isset($config['input_filter']) && is_array($config['input_filter'])) {
            $spec[$entity] = array_merge($spec[$entity], $config['input_filter']);
        }

        $factory = new \Zend\InputFilter\Factory();
        return $factory->createInputFilter($spec);
    }

    /**
     * Get viewport
     *
     * @param string $viewport
     * @param string $entity
     * @param string $path
     * @return array|null
     */
    public function getViewport($viewport, $entity, $path)
    {
        if (!isset($this->entities[$entity]['viewports'][$viewport])) {
            return null;
        }

        $repository = $this->getEntityManager()
                ->getRepository($this->getEntityClass($entity));

        if (null === $file = $repository->findOneByPath($path)) {
            return null;
        }

        $filesystem = $this->getEntityFilesystem($entity);
        $path = $entity . '/' . $viewport . '/' . $file->getPath();

        if (!$filesystem->has($path)) {
            $config = $this->entities[$entity]['viewports'][$viewport];

            if (!isset($config['width']) || !isset($config['width'])) {
                return null;
            }

            $path = $entity . '/' . $file->getPath();

            if (!$filesystem->has($path)) {
                return null;
            }

            $path = $this->createViewport($file, $viewport, $config['width'], $config['height']);
        }

        $hydrator = new ClassMethods();
        $size = $filesystem->getSize($path);

        $data = $hydrator->extract($file);
        $data['contents'] = $filesystem->readStream($path);
        $data['path'] = $path;
        $data['size'] = $size;

        return $data;
    }

    /**
     * Create viewport
     *
     * @param FileInterface $file
     * @param string $viewport
     * @param int $width
     * @param int $height
     * @return string
     */
    protected function createViewport(FileInterface $file, $viewport, $width, $height)
    {
        $entity = $this->getEntityName(get_class($file));
        $filesystem = $this->getEntityFilesystem($entity);
        $path = $entity . '/' . $file->getPath();

        $imagick = new \Imagick();
        $imagick->readimagefile($filesystem->readStream($path));
        $imagick->cropthumbnailimage($width, $height);

        $resource = fopen('php://temp', 'wb+');
        $imagick->writeimagesfile($resource);

        $path = $entity . '/' . $viewport . '/' . $file->getPath();
        $filesystem->writeStream($path, $resource);

        fclose($resource);
        $imagick->destroy();

        return $path;
    }

    /**
     * Persist
     *
     * @param FileInterface $file
     * @param string|resource $contents
     * @param bool $flush
     * @return FileInterface
     * @throws \InvalidArgumentException
     */
    public function persist(FileInterface $file, $contents, $flush = true)
    {
        $entity = get_class($file);
        $filesystem = $this->getEntityFilesystem($entity);

        if (is_resource($contents)) {
            rewind($contents);
            $contents = stream_get_contents($contents);
        }

        if (!is_string($contents)) {
            $type = is_object($contents) ? get_class($contents) : gettype($contents);
            $message = sprintf('Argument "contents" in %s must be a string or a stream resource; "%s" was given.', __METHOD__, $type);
            throw new \InvalidArgumentException($message);
        }

        $path = $this->getEntityName($entity) . '/' . $file->getPath();

        if (false === $result = $filesystem->write($path, $contents)) {
            $message = sprintf('Unable to write file "%s" in "%s"..', $path, __METHOD__);
            throw new \RuntimeException($message);
        }

        if ($file->getSize() === null) {
            $file->setSize($filesystem->getSize($path));
        }

        if ($file->getMimetype() === null) {
            $file->setMimetype($filesystem->getMimetype($path));
        }

        $this->getEntityManager()->persist($file);

        if ($flush) {
            $this->getEntityManager()->flush();
        }

        return $file;
    }

    /**
     * Remove
     *
     * @param FileInterface $file
     * @param bool $flush
     */
    public function remove(FileInterface $file, $flush = true)
    {
        $entity = get_class($file);
        $filesystem = $this->getEntityFilesystem($entity);

        $path = $this->getEntityName($entity) . '/' . $file->getPath();
        $filesystem->delete($path);

        $this->getEntityManager()->remove($file);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Get url
     *
     * @param FileInterface $file
     * @return string|null
     */
    public function getUrl(FileInterface $file)
    {
        $entity = get_class($file);

        if (is_subclass_of($entity, \Doctrine\ORM\Proxy\Proxy::class)) {
            $entity = get_parent_class($entity);
        }

        $config = $this->getEntity($entity);

        $baseUrl = isset($config['base_url']) ? $config['base_url'] : '';
        $url = sprintf('%s/%s/%s', trim($baseUrl, '/'), $this->getEntityName($entity), $file->getPath());

        return $url;
    }

}
