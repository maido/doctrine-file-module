<?php

namespace DoctrineFileModule\Service\File;

use DoctrineFileModule\Entity\FileInterface;

interface FileServiceInterface
{

    /**
     * Get entity manager
     *
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager();

    /**
     * Get mount manager
     *
     * @return \League\Flysystem\MountManager
     */
    public function getMountManager();

    /**
     * Get entities
     *
     * @return array
     */
    public function getEntities();

    /**
     * Has entity
     *
     * @param string $name
     * @return bool
     */
    public function hasEntity($name);

    /**
     * Get entity
     *
     * @param string $name
     * @return array
     * @throws \InvalidArgumentException
     */
    public function getEntity($name);

    /**
     * Get entity class
     *
     * @param string $name
     * @return string
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function getEntityClass($name);

    /**
     * Get entity filesystem
     *
     * @param string $name
     * @return \League\Flysystem\FilesystemInterface
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function getEntityFilesystem($name);

    /**
     * Get
     *
     * @param string $entity
     * @param string $path
     * @return array|null
     */
    public function get($entity, $path);

    /**
     * Persist
     *
     * @param FileInterface $file
     * @param string|resource $contents
     * @param bool $flush
     * @return FileInterface
     * @throws \InvalidArgumentException
     */
    public function persist(FileInterface $file, $contents, $flush = true);

    /**
     * Remove
     *
     * @param FileInterface $file
     * @param bool $flush
     */
    public function remove(FileInterface $file, $flush = true);
}
