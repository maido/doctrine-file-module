<?php

namespace DoctrineFileModule\Service\File;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Doctrine\ORM\EntityManager;
use League\Flysystem\MountManager;

class FileServiceFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $services
     * @return \DoctrineFileModule\Service\File\FileService
     */
    public function createService(ServiceLocatorInterface $services)
    {
        $entityManager = $services->get(EntityManager::class);
        $mountManager = $services->get(MountManager::class);

        $config = $services->get('config');
        $entities = isset($config['doctrine-file']['entities']) && is_array($config['doctrine-file']['entities']) ?
                $config['doctrine-file']['entities'] : [];

        $fileService = new FileService($entityManager, $mountManager, $entities);

        return $fileService;
    }
}
