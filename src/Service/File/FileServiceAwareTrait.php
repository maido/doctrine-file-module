<?php

namespace DoctrineFileModule\Service\File;

trait FileServiceAwareTrait
{

    /**
     * @var \DoctrineFileModule\Service\File\FileServiceInterface
     */
    protected $fileService;

    /**
     * Set file service
     *
     * @param \DoctrineFileModule\Service\File\FileServiceInterface $fileService
     * @return self
     */
    public function setFileService(FileServiceInterface $fileService)
    {
        $this->fileService = $fileService;
        return $this;
    }

    /**
     * Get file service
     *
     * @return \DoctrineFileModule\Service\File\FileServiceInterface
     */
    public function getFileService()
    {
        return $this->fileService;
    }
}
